//Framework Arduino
#include <Arduino.h>
//Library BLE par neil KOLBAN mode UART
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
//Library I2C pour GY521
#include <Wire.h>

//BLE
const char *UUID = "6E400001-B5A3-F393-E0A9-E50E24DCCA9E";
const char *ble_name = "ARC";
bool deviceConnected = false;
bool oldDeviceConnected = false;
BLEServer *pServer = NULL;
BLECharacteristic *pTxCharacteristic;
uint8_t buffer[5];
char payload[80];
int taille;

//GPIO DEFINITION
short g0 = 33;
short g1 = 32;
short g2 = 35;
short g3 = 39;
short g4 = 36;
short led = 18;
short batt = 34;
short tilt = 13;

//TILT
uint8_t lastRead;
uint32_t compteur;
uint8_t flag;

//FSR Variable
int val_g0, val_g1, val_g2, val_g3, val_g4;
const int echantillon = 75;

//For generating UUID https://www.uuidgenerator.net/
#define SERVICE_UUID UUID // UART service UUID
#define CHARACTERISTIC_UUID_RX UUID
#define CHARACTERISTIC_UUID_TX UUID

//Accelerometer & Gyroscope
const int MPU_addr = 0x68; // I2C address of the MPU-6050
int16_t AcX, AcY, AcZ, GyX, GyY, GyZ;
int minVal = 265;
int maxVal = 402;
double x;
double y;
double z;

//Prototypes des fonctions
String mesure_batt();
void envoi_ble(String message);
void get_gy521_angle();
void get_gy521();
void moyenne_gauge(int echantillon);
void vibration();
String parser_ang(double x, double y, double z);
String parser_tot(int16_t j0, int16_t j1, int16_t j2, int16_t j3, int16_t j4, int16_t Ax, int16_t Ay, int16_t Az, int16_t Gx, int16_t Gy, int16_t Gz);

//Retourne un boléen false BLE -->OFF / true BLE --> ON
class MyServerCallbacks : public BLEServerCallbacks
{
  void onConnect(BLEServer *pServer)
  {
    deviceConnected = true;
    Serial.println("connect");
  };

  void onDisconnect(BLEServer *pServer)
  {
    Serial.println("Disconnect");
    deviceConnected = false;
  }
};
//Message BLE entrant & sortant
class MyCallbacks : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string rxValue = pCharacteristic->getValue();
    if (rxValue.length() > 0)
    {
      for (int i = 0; i < rxValue.length(); i++)
        //Serial.print(rxValue[i]);
        buffer[i] = rxValue[i];
      taille = rxValue.length();
    }
  }
};

void setup()
{
  //PIN DEFINITION & Serial initialisation
  pinMode(led, OUTPUT);
  pinMode(g0, INPUT);
  pinMode(g1, INPUT);
  pinMode(g2, INPUT);
  pinMode(g3, INPUT);
  pinMode(g4, INPUT);
  pinMode(tilt, INPUT);
  digitalWrite(led, HIGH);
  Serial.begin(115200);

  //TILT initialisation
  flag = 1;

  compteur = 0;

  //GY521 Setting & initialisation
  Wire.begin(23, 22);
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);

  // Create the BLE Device
  BLEDevice::init(ble_name);
  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());
  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);
  // Create a BLE Characteristic
  pTxCharacteristic = pService->createCharacteristic(
      CHARACTERISTIC_UUID_TX,
      BLECharacteristic::PROPERTY_NOTIFY);
  pTxCharacteristic->addDescriptor(new BLE2902());
  BLECharacteristic *pRxCharacteristic = pService->createCharacteristic(
      CHARACTERISTIC_UUID_RX,
      BLECharacteristic::PROPERTY_WRITE);
  pRxCharacteristic->setCallbacks(new MyCallbacks());
  // Start the service
  pService->start();
  // Start advertising
  pServer->getAdvertising()->start();
}

void loop()

{
  if (deviceConnected)
  {
    if (taille == 0)
    {
      Serial.println("Pour test la fonction Jauges envoyer 1 charactere via le BLE");
      Serial.println("Pour test la fonction Gyroscope envoyer 2 charactere via le BLE");
      Serial.println("Pour test la fonction Vibration envoyer 3 charactere via le BLE");
      Serial.println("Pour test la fonction Batterie envoyer 4 charactere via le BLE");
      delay(10000);
    }
    if (taille == 1)
    {
      moyenne_gauge(echantillon);
      envoi_ble(String(val_g0) + "/" + String(val_g1) + "/" + String(val_g2) + "/" + String(val_g3) + "/" + String(val_g4));
      Serial.println("OK JAUGES");
    }
    if (taille == 2)
    {
      get_gy521_angle();
      envoi_ble(parser_ang(x,y,z));
      Serial.println("OK ANGLE GY521");
      delay(50);
    }
    if (taille == 3)
    {

      if (millis() % 5000 > 0)
      {
        vibration();
        flag = 0;
      }
      else if ((millis() % 5000 == 0) && (flag == 0))
      {
        envoi_ble(String(compteur));
        Serial.println("OK VIBRATION");
        compteur = 0;
        flag = 1;
      }
    }
    if (taille == 4)
    {
      moyenne_gauge(echantillon);
      get_gy521();
      envoi_ble(parser_tot(val_g0, val_g1, val_g2, val_g3, val_g4, AcX, AcY, AcZ, GyX, GyY, GyZ));
      Serial.println("OK TOT jauge& GY521");
    }
    if (taille == 5)
    {
      envoi_ble(mesure_batt());
      delay(75);
      Serial.println("OK BATT");
    }
  }
  else
  {
    Serial.println("En attente de connection...");
    delay(10000);
  }
}

String mesure_batt()
{
  const float tot = 825;
  const float max = 2450;
  float ratio = ((max - analogRead(batt)) / tot);
  int t = (1.00 - ratio) * 100.00;
  String taux = String(t);
  return taux;
}

void envoi_ble(String message)
{
  message.toCharArray(payload, 80);
  pTxCharacteristic->setValue(payload);
  pTxCharacteristic->notify();
}
void get_gy521()
{
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x3B); // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr, 14, true); // request a total of 14 registers
  AcX = Wire.read() << 8 | Wire.read(); // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
  AcY = Wire.read() << 8 | Wire.read(); // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ = Wire.read() << 8 | Wire.read(); // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  GyX = Wire.read() << 8 | Wire.read(); // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  GyY = Wire.read() << 8 | Wire.read(); // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  GyZ = Wire.read() << 8 | Wire.read(); // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
}

void get_gy521_angle()
{
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x3B); // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr, 14, true); // request a total of 14 registers
  AcX = Wire.read() << 8 | Wire.read(); // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
  AcY = Wire.read() << 8 | Wire.read(); // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ = Wire.read() << 8 | Wire.read(); // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  GyX = Wire.read() << 8 | Wire.read(); // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  GyY = Wire.read() << 8 | Wire.read(); // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  GyZ = Wire.read() << 8 | Wire.read(); // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
  int xAng = map(AcX, minVal, maxVal, -90, 90);
  int yAng = map(AcY, minVal, maxVal, -90, 90);
  int zAng = map(AcZ, minVal, maxVal, -90, 90);

  x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
  y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
  z = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);
}
void moyenne_gauge(int echantillon)
{
  int sum0 = 0, sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0;
  for (int i = 0; i <= echantillon; i++)
  {
    sum0 += analogRead(g0);
    sum1 += analogRead(g1);
    sum2 += analogRead(g2);
    sum3 += analogRead(g3);
    sum4 += analogRead(g4);
    delay(1);
    if (i == echantillon)
    {
      val_g0 = sum0 / echantillon;
      val_g1 = sum1 / echantillon;
      val_g2 = sum2 / echantillon;
      val_g3 = sum3 / echantillon;
      val_g4 = sum4 / echantillon;
    }
  }
}

String parser_ang(double x, double y, double z)
{

  String chaine = String(x) + "/" + String(y) + "/" + String(z);
  return chaine;
}
String parser_tot(int16_t j0, int16_t j1, int16_t j2, int16_t j3, int16_t j4, int16_t Ax, int16_t Ay, int16_t Az, int16_t Gx, int16_t Gy, int16_t Gz)
{

  String chaine = String(j0) + "/" + String(j1) + "/" + String(j2) + "/" + String(j3) + "/" + String(j4) + "/" + String(Ax) + "/" + String(Ay) + "/" + String(Az) + "/" + String(Gx) + "/" + String(Gy) + "/" + String(Gz);
  return chaine;
}

void vibration()
{
  int8_t read = digitalRead(tilt);
  if (read != lastRead)
  {
    lastRead = read;
    compteur = compteur + 1;
  }
}
